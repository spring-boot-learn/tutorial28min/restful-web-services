package cz.ill_j.springboot.rest.webservices.restfulwebservices.web_controller;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.MessageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController 
public class HelloWorldController {
    
    @Autowired
    MessageSource messageSource;
    
    @RequestMapping(method = RequestMethod.GET, path = "/hello-world")
    public String helloWorld() {
        return MessageBean.HELLO_WORLD;
    }

    @GetMapping(path = "/hello-world-internationalized")
    public String helloWorldInternationalized() {
        return messageSource.getMessage("greeting", null, LocaleContextHolder.getLocale());
    }
}
