package cz.ill_j.springboot.rest.webservices.restfulwebservices;

public class Main {
    
    private static String base = "basic-registers.";
    private static String[] array = {"documentNumber", "documentType"};
    
    public static void main(String[] args) {
//        for (String key : array) {
//            System.out.print(getTranslate(key));
//        }
        
        new Main().setLists();
    }
    
    private static String getTranslate(String key) {
        String translate = ",\n\"" + base + key + "\": {\n\t\"value\": \"" + key + "\"\n}";
        return translate;
    }


    /**
     * Druh zmeny: N=Nova Z=Zmena V=Vymaz
     */
    public enum DruhZmeny
    {
        N, Z, V
    }

    private enum TypVratky
    {
        VEX, VRD
    }

    private enum DuvodVratky
    {
        O1(1), O2(2), O3(3),
        A, B, C, D, J, K, L, M, N, O, P, R, S, T, U, V, W, Z;

        private int value;

        DuvodVratky() {

        }

        DuvodVratky(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    private void setLists() {
        System.out.println(DruhZmeny.values());
        System.out.println(TypVratky.values()); // CK_TYP_VRATKY
        System.out.println(DuvodVratky.values()); // CK_DUVOD_VRATKY
        
    }
}
