package cz.ill_j.springboot.rest.webservices.restfulwebservices.web_controller;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.MessageBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Message {

    @GetMapping (path = "/message/path/{name}")
    public MessageBean getMessage(@PathVariable String name) {
        return new MessageBean(String.format(MessageBean.HELLO_WORLD + ", %s", name));
    }
}
