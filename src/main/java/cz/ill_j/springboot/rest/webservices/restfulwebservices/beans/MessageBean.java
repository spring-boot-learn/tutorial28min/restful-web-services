package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans;

public class MessageBean {

    public static final String HELLO_WORLD = "Hello World";
    private String message;

    public MessageBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageBean{" +
                "message='" + message + '\'' +
                '}';
    }
}
