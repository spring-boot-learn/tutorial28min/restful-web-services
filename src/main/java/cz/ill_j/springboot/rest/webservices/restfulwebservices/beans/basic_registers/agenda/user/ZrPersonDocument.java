package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

public class ZrPersonDocument {

    private int id;
    private int documentNumber;
    private String documentType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(int documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
