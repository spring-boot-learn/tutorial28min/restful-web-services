package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

public class ZrLegalState {
    
    private int id;
    private int code;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
