package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda;

public class Ustanoveni {
    
    private int id;
    private String cisloPravnihoPredpisu;
    private int rokPravnihoPredpisu;
    private String nazevPravnihoPredpisu;
    private String paragrafUstanoveni;
    private String odstavecUstanoveni;
    private String pisemnostUstanoveni;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCisloPravnihoPredpisu() {
        return cisloPravnihoPredpisu;
    }

    public void setCisloPravnihoPredpisu(String cisloPravnihoPredpisu) {
        this.cisloPravnihoPredpisu = cisloPravnihoPredpisu;
    }

    public int getRokPravnihoPredpisu() {
        return rokPravnihoPredpisu;
    }

    public void setRokPravnihoPredpisu(int rokPravnihoPredpisu) {
        this.rokPravnihoPredpisu = rokPravnihoPredpisu;
    }

    public String getNazevPravnihoPredpisu() {
        return nazevPravnihoPredpisu;
    }

    public void setNazevPravnihoPredpisu(String nazevPravnihoPredpisu) {
        this.nazevPravnihoPredpisu = nazevPravnihoPredpisu;
    }

    public String getParagrafUstanoveni() {
        return paragrafUstanoveni;
    }

    public void setParagrafUstanoveni(String paragrafUstanoveni) {
        this.paragrafUstanoveni = paragrafUstanoveni;
    }

    public String getOdstavecUstanoveni() {
        return odstavecUstanoveni;
    }

    public void setOdstavecUstanoveni(String odstavecUstanoveni) {
        this.odstavecUstanoveni = odstavecUstanoveni;
    }

    public String getPisemnostUstanoveni() {
        return pisemnostUstanoveni;
    }

    public void setPisemnostUstanoveni(String pisemnostUstanoveni) {
        this.pisemnostUstanoveni = pisemnostUstanoveni;
    }
}
