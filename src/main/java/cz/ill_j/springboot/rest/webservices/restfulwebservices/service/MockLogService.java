package cz.ill_j.springboot.rest.webservices.restfulwebservices.service;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs.LogData;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs.MockLog;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MockLogService {
    
    public MockLog getMockLogList() {
        MockLog mockLog = new MockLog();
        for (int i = 0; i < 10; i++) {
            LogData data = new LogData();
            data.setId(i + 1);
            data.setUid(i+10);
            data.setPrevious("Previous " + i);
            data.setCas_zadosti(new Date());
            data.setAgenda("Agenda " + i);
            data.setAgendova_role("Role " + i);
            data.setUzivatel("user " + i);
            data.setSubjekt("sub0" + i);
            data.setDuvod("nic moc");
            data.setSluzba("ahoj sluzbo");
            data.setOperace("operace " + i);
            
            mockLog.getPageData().add(data);
        }
        
        return mockLog;
    }
}
