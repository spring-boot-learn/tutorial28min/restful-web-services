package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs;

import java.util.Date;

public class LogData {
    
    private int id;
    private int uid;
    private String previous;
    private Date cas_zadosti;
    private String agenda;
    private String agendova_role;
    private String uzivatel;
    private String subjekt;
    private String duvod;
    private String sluzba;
    private String operace;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public Date getCas_zadosti() {
        return cas_zadosti;
    }

    public void setCas_zadosti(Date cas_zadosti) {
        this.cas_zadosti = cas_zadosti;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public String getAgendova_role() {
        return agendova_role;
    }

    public void setAgendova_role(String agendova_role) {
        this.agendova_role = agendova_role;
    }

    public String getUzivatel() {
        return uzivatel;
    }

    public void setUzivatel(String uzivatel) {
        this.uzivatel = uzivatel;
    }

    public String getSubjekt() {
        return subjekt;
    }

    public void setSubjekt(String subjekt) {
        this.subjekt = subjekt;
    }

    public String getDuvod() {
        return duvod;
    }

    public void setDuvod(String duvod) {
        this.duvod = duvod;
    }

    public String getSluzba() {
        return sluzba;
    }

    public void setSluzba(String sluzba) {
        this.sluzba = sluzba;
    }

    public String getOperace() {
        return operace;
    }

    public void setOperace(String operace) {
        this.operace = operace;
    }
}
