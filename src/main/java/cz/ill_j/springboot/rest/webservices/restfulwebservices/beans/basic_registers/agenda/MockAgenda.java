package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda;

import java.util.ArrayList;
import java.util.List;

public class MockAgenda {

    private int countPages = 1;
    private int countAll = 10;

    private List<AgendaData> pageData = new ArrayList();

    public int getCountPages() {
        return countPages;
    }

    public int getCountAll() {
        return countAll;
    }

    public List<AgendaData> getPageData() {
        return pageData;
    }
}
