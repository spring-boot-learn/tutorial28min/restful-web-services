package cz.ill_j.springboot.rest.webservices.restfulwebservices.service;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.AgendaCinnost;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.AgendaData;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.MockAgenda;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.Opravneni;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.Ustanoveni;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;

@Component
public class MockAgendaService {

    private Random random = new Random();

    public MockAgenda getMockAgendaList() {
        MockAgenda agenda = new MockAgenda();
        for (int i = 0; i < agenda.getCountAll(); i++) {
            agenda.getPageData().add(createAgendaData(i));
        }
        return agenda;
    }
    
    private AgendaData createAgendaData(int i) {
        AgendaData data = new AgendaData();
        data.setId(i + 1);
        data.setKod("kod_0" + data.getId());
        data.setNazev("nazev_0" + data.getId());
        data.setDatumPlatnostiOd(new Date());
        data.setDatumPlatnostiDo(new Date());
        for (int j = 0; j < random.nextInt(5); j++) {
            data.getAgendaCinnost().add(createAgendaCinnost(random.nextInt()));
        }
        data.setKodRegistrujicihoOvm("reg_" + (random.nextInt(18) * i));
        data.setStav("stav" + i);
        return data;
    }
    
    private AgendaCinnost createAgendaCinnost(int id) {
        AgendaCinnost cinnost = new AgendaCinnost();
        cinnost.setId(id);
        cinnost.setKod("kod_cinnost_" + id);
        cinnost.setNazev("nazev_" + id * 2);
        cinnost.setPopis("popis_" + id * 5);
        cinnost.setDatumPlatnostiOd(new Date());
        cinnost.setDatumPlatnostiDo(new Date());
        for (int k = 0; k < random.nextInt(4); k++) {
            cinnost.getOpravneni().add(createOpravneni(k));
        }
        return cinnost;
    }

    private Opravneni createOpravneni(int k) {
        Opravneni opravneni = new Opravneni();
        opravneni.setId(k);
        opravneni.setOpravneni("opravneni_" + k);
        for (int j = 1; j < random.nextInt(9); j++) {
            opravneni.getUstanoveni().add(createUstanoveni(j));
        }
        return opravneni;
    }

    private Ustanoveni createUstanoveni(int id) {
        Ustanoveni ustanoveni = new Ustanoveni();
        ustanoveni.setId(id);
        ustanoveni.setCisloPravnihoPredpisu("cisloPravnihoPredpisu_" + id);
        ustanoveni.setNazevPravnihoPredpisu("nazevPravnihoPredpisu_" + id);
        ustanoveni.setPisemnostUstanoveni("pisemnost_" + (id*10));
        return ustanoveni;
    }
}
