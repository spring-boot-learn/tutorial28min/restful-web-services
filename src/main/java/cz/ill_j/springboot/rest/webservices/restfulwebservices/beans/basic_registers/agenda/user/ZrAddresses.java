package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

public class ZrAddresses {
    
    private int id;
    private int adresniMistoKod;
    private int castObceKod;
    private String casObceNazev;
    private int cisloDomovni;
    private int cisloOrientacni;
    private String cisloOrientacniPismeno;
    private int obecKod;
    private String obecNazev;
    private int okresKod;
    private String okresNazev;
    private int postaKod;
    private String postaNazev;
    private int stavebniObjektKod;
    private int typCislaDomovnihoKod;
    private int uliceKod;
    private String uliceNazev;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdresniMistoKod() {
        return adresniMistoKod;
    }

    public void setAdresniMistoKod(int adresniMistoKod) {
        this.adresniMistoKod = adresniMistoKod;
    }

    public int getCastObceKod() {
        return castObceKod;
    }

    public void setCastObceKod(int castObceKod) {
        this.castObceKod = castObceKod;
    }

    public String getCasObceNazev() {
        return casObceNazev;
    }

    public void setCasObceNazev(String casObceNazev) {
        this.casObceNazev = casObceNazev;
    }

    public int getCisloDomovni() {
        return cisloDomovni;
    }

    public void setCisloDomovni(int cisloDomovni) {
        this.cisloDomovni = cisloDomovni;
    }

    public int getCisloOrientacni() {
        return cisloOrientacni;
    }

    public void setCisloOrientacni(int cisloOrientacni) {
        this.cisloOrientacni = cisloOrientacni;
    }

    public String getCisloOrientacniPismeno() {
        return cisloOrientacniPismeno;
    }

    public void setCisloOrientacniPismeno(String cisloOrientacniPismeno) {
        this.cisloOrientacniPismeno = cisloOrientacniPismeno;
    }

    public int getObecKod() {
        return obecKod;
    }

    public void setObecKod(int obecKod) {
        this.obecKod = obecKod;
    }

    public String getObecNazev() {
        return obecNazev;
    }

    public void setObecNazev(String obecNazev) {
        this.obecNazev = obecNazev;
    }

    public int getOkresKod() {
        return okresKod;
    }

    public void setOkresKod(int okresKod) {
        this.okresKod = okresKod;
    }

    public String getOkresNazev() {
        return okresNazev;
    }

    public void setOkresNazev(String okresNazev) {
        this.okresNazev = okresNazev;
    }

    public int getPostaKod() {
        return postaKod;
    }

    public void setPostaKod(int postaKod) {
        this.postaKod = postaKod;
    }

    public String getPostaNazev() {
        return postaNazev;
    }

    public void setPostaNazev(String postaNazev) {
        this.postaNazev = postaNazev;
    }

    public int getStavebniObjektKod() {
        return stavebniObjektKod;
    }

    public void setStavebniObjektKod(int stavebniObjektKod) {
        this.stavebniObjektKod = stavebniObjektKod;
    }

    public int getTypCislaDomovnihoKod() {
        return typCislaDomovnihoKod;
    }

    public void setTypCislaDomovnihoKod(int typCislaDomovnihoKod) {
        this.typCislaDomovnihoKod = typCislaDomovnihoKod;
    }

    public int getUliceKod() {
        return uliceKod;
    }

    public void setUliceKod(int uliceKod) {
        this.uliceKod = uliceKod;
    }

    public String getUliceNazev() {
        return uliceNazev;
    }

    public void setUliceNazev(String uliceNazev) {
        this.uliceNazev = uliceNazev;
    }
}
