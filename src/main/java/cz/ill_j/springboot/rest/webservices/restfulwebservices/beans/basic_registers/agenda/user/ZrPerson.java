package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ZrPerson {
    
    private int id;
    private String aifoLocal;
    private String aifoGlobal;
    private String firstname;
    private String surname;
    private Date birthdate;
    private Date deathdate;
    private int ico;
    private int personIco;
    private String personType;
    private String citizenship;
    private List<ZrAddresses> zrAddresses = new ArrayList();
    private List<ZrPersonDocument> zrPersonDocuments = new ArrayList();
    private List<ZrDatabox> zrDataboxes = new ArrayList();
    private List<ZrLegalForm> zrLegalForms = new ArrayList();
    private List<ZrLegalState> zrLegalStates = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAifoLocal() {
        return aifoLocal;
    }

    public void setAifoLocal(String aifoLocal) {
        this.aifoLocal = aifoLocal;
    }

    public String getAifoGlobal() {
        return aifoGlobal;
    }

    public void setAifoGlobal(String aifoGlobal) {
        this.aifoGlobal = aifoGlobal;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Date getDeathdate() {
        return deathdate;
    }

    public void setDeathdate(Date deathdate) {
        this.deathdate = deathdate;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public int getPersonIco() {
        return personIco;
    }

    public void setPersonIco(int personIco) {
        this.personIco = personIco;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public List<ZrAddresses> getZrAddresses() {
        return zrAddresses;
    }

    public void setZrAddresses(List<ZrAddresses> zrAddresses) {
        this.zrAddresses = zrAddresses;
    }

    public List<ZrPersonDocument> getZrPersonDocuments() {
        return zrPersonDocuments;
    }

    public void setZrPersonDocuments(List<ZrPersonDocument> zrPersonDocuments) {
        this.zrPersonDocuments = zrPersonDocuments;
    }

    public List<ZrDatabox> getZrDataboxes() {
        return zrDataboxes;
    }

    public void setZrDataboxes(List<ZrDatabox> zrDataboxes) {
        this.zrDataboxes = zrDataboxes;
    }

    public List<ZrLegalForm> getZrLegalForms() {
        return zrLegalForms;
    }

    public void setZrLegalForms(List<ZrLegalForm> zrLegalForms) {
        this.zrLegalForms = zrLegalForms;
    }

    public List<ZrLegalState> getZrLegalStates() {
        return zrLegalStates;
    }

    public void setZrLegalStates(List<ZrLegalState> zrLegalStates) {
        this.zrLegalStates = zrLegalStates;
    }
}
