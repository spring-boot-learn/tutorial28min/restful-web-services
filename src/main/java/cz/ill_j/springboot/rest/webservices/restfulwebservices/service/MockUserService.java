package cz.ill_j.springboot.rest.webservices.restfulwebservices.service;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.MockUserList;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.ZrAddresses;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.ZrDatabox;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.ZrPerson;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.ZrPersonDocument;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
public class MockUserService {

    private static Random generate = new Random();

    private String[] firstNameList = {"John", "Marcus", "Susan", "Henry", "Bob", "Jill", "Tom", "Brandon", "Dylan"};
    private String[] lastNameList = {"Jim", "Fred", "Baz", "Bing", "Duck", "Swan", "Cooper", "Impuls", "York"};

    public MockUserList getZrPersonMockUserList() {
        MockUserList mockUserList = new MockUserList();
        for (int i = 0; i < mockUserList.getCountAll(); i++) {
            mockUserList.getPageData().add(createZrPerson(i+1));
        }
        
        return mockUserList;
    }

    private ZrPerson createZrPerson(int id) {
        ZrPerson zrPerson = new ZrPerson();
        // setting person data
        zrPerson.setId(id);
        zrPerson.setFirstname(getRandomFirstName());
        zrPerson.setSurname(getRandomLastName());
        zrPerson.setBirthdate(new Date());
        zrPerson.setAifoGlobal("aifoGlobal_" + id);
        zrPerson.setAifoLocal("aifoLocal_" + id);
        // setting address data
        zrPerson.setZrAddresses(createZrAddressList(generate.nextInt(4)));
        zrPerson.setZrPersonDocuments(createZrPersonDocumentList(generate.nextInt(6)));
        zrPerson.setZrDataboxes(createZrDataboxList(generate.nextInt(3)));
        
        return zrPerson;
    }

    private List<ZrAddresses> createZrAddressList(int listLength) {
        List<ZrAddresses> zrAddresses = new ArrayList();
        for (int i = 0; i < listLength; i++) {
            zrAddresses.add(createZrAddress(i+1));
        }
        return zrAddresses;
    }

    private ZrAddresses createZrAddress(int id) {
        ZrAddresses zrAddress = new ZrAddresses();
        zrAddress.setId(id);
        zrAddress.setUliceNazev("Ulice " + generate.nextInt(50));
        zrAddress.setCisloDomovni(generate.nextInt(100));
        zrAddress.setObecNazev("Lanzhot");
        return zrAddress;
    }

    private List<ZrPersonDocument> createZrPersonDocumentList(int listLength) {
        List<ZrPersonDocument> zrPersonDocuments = new ArrayList();
        for (int i = 0; i < listLength; i++) {
            zrPersonDocuments.add(createZrPersonDocument(i+1));
        }
        return zrPersonDocuments;
    }

    private ZrPersonDocument createZrPersonDocument(int id) {
        ZrPersonDocument zrPersonDocument = new ZrPersonDocument();
        zrPersonDocument.setId(id);
        zrPersonDocument.setDocumentNumber(id* generate.nextInt(100));
        zrPersonDocument.setDocumentType("type_" + getRandomLastName());
        return zrPersonDocument;
    }

    private List<ZrDatabox> createZrDataboxList(int listLength) {
        List<ZrDatabox> zrDataboxes = new ArrayList();
        for (int i = 0; i < listLength; i++) {
            zrDataboxes.add(createZrDatabox(i+1));
        }
        return zrDataboxes;
    }

    private ZrDatabox createZrDatabox(int id) {
        ZrDatabox zrDatabox = new ZrDatabox();
        zrDatabox.setId(id*generate.nextInt(25));
        zrDatabox.setName(getRandomFirstName() + zrDatabox.getId());
        return zrDatabox;
    }

    public String getRandomFirstName() {
        return firstNameList[generate.nextInt(9)];
    }
    
    public String getRandomLastName() {
        return lastNameList[generate.nextInt(9)];
    }
}
