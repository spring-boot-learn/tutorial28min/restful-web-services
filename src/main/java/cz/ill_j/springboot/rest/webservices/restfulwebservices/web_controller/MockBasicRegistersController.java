package cz.ill_j.springboot.rest.webservices.restfulwebservices.web_controller;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.MockAgenda;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs.MockLog;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user.MockUserList;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.service.MockAgendaService;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.service.MockLogService;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.service.MockUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MockBasicRegistersController {
    
    @Autowired
    private MockUserService userService;
    @Autowired
    private MockAgendaService agendaService;
    @Autowired
    private MockLogService logService;

    @GetMapping("/basic-registers/userList")
    public ResponseEntity<MockUserList> getMockUsers() {
        MockUserList mockUser = userService.getZrPersonMockUserList();
        if (mockUser == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(mockUser);
        }
    }
    
    @GetMapping("/basic-registers/agendaList")
    public ResponseEntity<MockAgenda> getMockAgenda() {
        MockAgenda mockAgenda = agendaService.getMockAgendaList();
        if (mockAgenda == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(mockAgenda);
        }
    }

    @GetMapping("/basic-registers/loglist")
    public ResponseEntity<MockLog> getMockLog() {
        MockLog mockLog = logService.getMockLogList();
        if (mockLog == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(mockLog);
        }
    }
}
