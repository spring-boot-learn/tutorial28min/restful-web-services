package cz.ill_j.springboot.rest.webservices.restfulwebservices.dao;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class UserDaoService {
    
    private static List<User> list = new ArrayList();

    static {
        list.add(new User(1, "Tonda", new Date()));
        list.add(new User(2, "Jarka", new Date()));
        list.add(new User(3, "Karel", new Date()));
        list.add(new User(4, "Hynek", new Date()));
    };
    
    public User save(User user) {
        user.setId(getLastId() + 1);
        list.add(user);
        return user;
    }

    private int getLastId() {
        int lastId = 1;
        for (User u : list) {
            if(u.getId() > lastId) {
                lastId = u.getId();
            }
        }
        return lastId;
    }

    public List<User> findAll() {
        return list;
    }

    public User findOne(int id) {
        User user = null;
        for (User u : list) {
            if(u.getId() == id) {
                user = u;
            }
        }
        return user;
    }

    public User deleteById(int id) {
        Iterator<User> iterator = list.iterator();
        while (iterator.hasNext()) {
            User u = iterator.next();
            if(u.getId() == id) {
                iterator.remove();
                return u;
            }
        }
        return null;
    }
}
