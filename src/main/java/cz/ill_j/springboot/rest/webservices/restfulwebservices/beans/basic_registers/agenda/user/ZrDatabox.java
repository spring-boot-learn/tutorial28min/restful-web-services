package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

public class ZrDatabox {
    
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
