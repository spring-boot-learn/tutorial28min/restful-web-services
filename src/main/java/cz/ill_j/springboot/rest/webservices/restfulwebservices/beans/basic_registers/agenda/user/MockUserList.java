package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.user;

import java.util.ArrayList;
import java.util.List;

public class MockUserList {

    private int countPages = 1;
    private int countAll = 10;
    
    private List<ZrPerson> pageData = new ArrayList();

    public int getCountPages() {
        return countPages;
    }

    public void setCountPages(int countPages) {
        this.countPages = countPages;
    }

    public int getCountAll() {
        return countAll;
    }

    public void setCountAll(int countAll) {
        this.countAll = countAll;
    }

    public List<ZrPerson> getPageData() {
        return pageData;
    }

    public void setPageData(List<ZrPerson> pageData) {
        this.pageData = pageData;
    }
}
