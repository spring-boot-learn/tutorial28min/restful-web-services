package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda.logs.LogData;

import java.util.ArrayList;
import java.util.List;

public class MockLog {
    
    private int countPages = 1;
    private int countAll = 10;
    
    private List<LogData> pageData = new ArrayList();

    public int getCountPages() {
        return countPages;
    }

    public int getCountAll() {
        return countAll;
    }

    public List<LogData> getPageData() {
        return pageData;
    }
}
