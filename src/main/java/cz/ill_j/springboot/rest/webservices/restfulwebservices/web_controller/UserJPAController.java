package cz.ill_j.springboot.rest.webservices.restfulwebservices.web_controller;

import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.Post;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.PostRepository;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.User;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.UserRepository;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.dao.UserDaoService;
import cz.ill_j.springboot.rest.webservices.restfulwebservices.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class UserJPAController {

    @Autowired
    private UserDaoService service;
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/jpa/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/jpa/users/{id}")
    public EntityModel getUser(@PathVariable int id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException("id: " + id);
        }
        // link "all-users", SERVER_PATH + "/users"
        EntityModel<User> model = new EntityModel(user);
        WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).getAllUsers());
        model.add(linkTo.withRel("all-users"));

        return model;
    }

    @PostMapping("/jpa/users")
    public ResponseEntity<Object> createUser(@Valid  @RequestBody User user) {
        User savedUser = userRepository.save(user);
        // CREATED
        // /user/{id}
        
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedUser.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/jpa/users/{id}")
    public void deleteUserById(@PathVariable int id) {
        userRepository.deleteById(id);
    }
    
    // Posts
    @GetMapping("/jpa/users/{id}/posts")
    public List<Post> getAllUsersPosts(@PathVariable int id) {
        Optional<User> optionalUser = userRepository.findById(id);
        
        if(!optionalUser.isPresent()) {
            throw new UserNotFoundException("id-" + id);
        }
        return optionalUser.get().getPosts();
    }

    @PostMapping("/jpa/users/{id}/posts")
    public ResponseEntity<Object> createPost(@Valid @PathVariable int id,  @RequestBody Post post) {
        Optional<User> optionalUser = userRepository.findById(id);

        if(!optionalUser.isPresent()) {
            throw new UserNotFoundException("id-" + id);
        }
        User user = optionalUser.get();
        post.setUser(user);
        postRepository.save(post);
        
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(post.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
