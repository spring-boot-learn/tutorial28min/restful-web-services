package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgendaCinnost {
    
    private int id;
    private String kod;
    private String nazev;
    private String popis;
    private Date datumPlatnostiOd;
    private Date datumPlatnostiDo;
    private List<Opravneni> opravneni = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public Date getDatumPlatnostiOd() {
        return datumPlatnostiOd;
    }

    public void setDatumPlatnostiOd(Date datumPlatnostiOd) {
        this.datumPlatnostiOd = datumPlatnostiOd;
    }

    public Date getDatumPlatnostiDo() {
        return datumPlatnostiDo;
    }

    public void setDatumPlatnostiDo(Date datumPlatnostiDo) {
        this.datumPlatnostiDo = datumPlatnostiDo;
    }

    public List<Opravneni> getOpravneni() {
        return opravneni;
    }

    public void setOpravneni(List<Opravneni> opravneni) {
        this.opravneni = opravneni;
    }
}
