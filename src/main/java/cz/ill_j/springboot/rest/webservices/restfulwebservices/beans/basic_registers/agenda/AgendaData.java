package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgendaData {
    
    private int id;
    private String kod;
    private String nazev;
    private Date datumPlatnostiOd;
    private Date datumPlatnostiDo;
    private String kodRegistrujicihoOvm;
    private String stav;
    private List<AgendaCinnost> agendaCinnost = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public Date getDatumPlatnostiOd() {
        return datumPlatnostiOd;
    }

    public void setDatumPlatnostiOd(Date datumPlatnostiOd) {
        this.datumPlatnostiOd = datumPlatnostiOd;
    }

    public Date getDatumPlatnostiDo() {
        return datumPlatnostiDo;
    }

    public void setDatumPlatnostiDo(Date datumPlatnostiDo) {
        this.datumPlatnostiDo = datumPlatnostiDo;
    }

    public String getKodRegistrujicihoOvm() {
        return kodRegistrujicihoOvm;
    }

    public void setKodRegistrujicihoOvm(String kodRegistrujicihoOvm) {
        this.kodRegistrujicihoOvm = kodRegistrujicihoOvm;
    }

    public String getStav() {
        return stav;
    }

    public void setStav(String stav) {
        this.stav = stav;
    }

    public List<AgendaCinnost> getAgendaCinnost() {
        return agendaCinnost;
    }

    public void setAgendaCinnost(List<AgendaCinnost> agendaCinnost) {
        this.agendaCinnost = agendaCinnost;
    }
}
