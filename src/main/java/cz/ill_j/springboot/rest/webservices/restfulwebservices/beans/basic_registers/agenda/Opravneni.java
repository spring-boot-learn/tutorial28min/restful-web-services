package cz.ill_j.springboot.rest.webservices.restfulwebservices.beans.basic_registers.agenda;

import java.util.ArrayList;
import java.util.List;

public class Opravneni {
    
    private int id;
    private String opravneni;
    private List<Ustanoveni> ustanoveni = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpravneni() {
        return opravneni;
    }

    public void setOpravneni(String opravneni) {
        this.opravneni = opravneni;
    }

    public List<Ustanoveni> getUstanoveni() {
        return ustanoveni;
    }

    public void setUstanoveni(List<Ustanoveni> ustanoveni) {
        this.ustanoveni = ustanoveni;
    }
}
